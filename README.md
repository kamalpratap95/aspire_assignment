# Quasar App (aspire_assignment)

A Quasar Framework app

## Install the dependencies
```bash
npm install
```

### Start client - default port 8080
```bash
npm run client
```

### Start server - default port 3000
```bash
npm run server
```

#Overview
```
To mock database (/data/db.json) , I used json-server to serve a local json file and frontend is built in Quasar using Vue3. I am more comfortable working in Vue2 but to make the assignment a bit more challenging, I chose Vue3
```

#API response Overview
```
User :
    {
      "id": 1,
      "firstName": "kamal",
      "lastName": "pratap",
      "email": "kamal@abc.com",
      "mobile": "9996767805",
      "password": "sajdnjaksdnjsakd",
      "isActive": true
    }
 ```

```
Loan :
    {
      "id": 1,
      "name": "Flexi Personal Loan",
      "amount": "11000",
      "repaymentMonths": 3,
      "interestPerMonth": 2.4
    },
```

```
"purchased-loan-list": {
      "id": 1,
      "user":  {
        "id": 1,
        "firstName": "kamal",
        "lastName": "pratap",
        "email": "kamal@abc.com",
        "mobile": "9996767805",
        "password": "sajdnjaksdnjsakd",
        "isActive": true
      },
      "loan":  [
        {
          "id": 1,
          "name": "Flexi Personal Loan",
          "amount": "11000",
          "repaymentMonths": 3,
          "interestPerMonth": 2.4,
          "repaymentsMadeCount": 2,
          "purchaseDate":"2021-11-29T20:23:56.540Z"
        },
        {
          "id": 3,
          "amount": "20000",
          "name": "Personal Loan - Salaried",
          "repaymentMonths": 12,
          "interestPerMonth": 1.2,
          "repaymentsMadeCount": 2,
          "purchaseDate":"2021-11-30T20:23:56.540Z"
        }
      ]
    }
```

#Sign in and Sign up
1. go to http://localhost:8080/auth/register to create an account
   With form validations and only Indian phone numbers are accepted( eg: 9996767805)
   Hits a post api and entry is generated in db.json file under users.


2. go to http://localhost:8080/auth/login to login
   Assumed user-id: 1 is fetched from db everytime

#view all Loans
1. Click on `Loans` on the side menu
   It shows list of available loans with repayment duration, amount and interest rate per month.

2. Click on `Apply Now` button on any loan to preview schedule payments
3. Click on `Purchase` to buy loan

Note: It is not saved in database.

#View purchased loans
1. Click on `Payments` from the side menu.
   It shows list of purchased loans from a userId
Note: Same components are used in /loans and /payments page

2. Click on `View Payments` button on any card
from `purchased-loan-list` table in `db.json`, we can get list of all loans for a particular user with details mentioned like purchase date and how many payments have been made yet.

3. There we calculate the logic and shows a column with buttons displaying whether the payment for that duration was made or not.


I had my laser surgery 2 weeks before, and I am having difficulty in reading from close up. So, I tried my best to do the assignemnt.
Thanks!


