import payments from "../assets/sidenav/payments.svg"
import card from "../assets/sidenav/card.svg"
import credit from "../assets/sidenav/credit.svg"
import account from "../assets/sidenav/account.svg"
import dashboard from "../assets/logo/mobile.svg"

export default {
  data(){
    const sideNavItems= [
      { icon: dashboard, value:'Loan', path:'/loans'},
      { icon:card, value:'Cards', path:'/cards'},
      { icon:payments, value:'Payments', path:'/payments'},
      { icon:credit, value:'Credits', path:'/credits'},
      { icon:account, value:'Account', path:'/settings'}]
    return {
      sideNavItems
    }
  }
}
