export default {
  data(){
    return {
      columns:[
        { name: '#', align: 'left', label: '#', field: 'number', sortable: true },
        {
          name: 'amount',
          required: true,
          label: 'Principal per month',
          align: 'center',
          field: row => row.amount,
          format: val => `₹ ${val}`,
          sortable: true
        },
        { name: 'Interest', align: 'left', label: 'Interest', field: 'interestPerMonth', sortable: true, format: val => `₹ ${val}` },
        { name: 'dueDate', align: 'left', label: 'Due date', field: 'dueDate', sortable: true },
        { name: 'interest', align: 'left', label: 'Repayable Amount', field: 'repayableAmount', sortable: true, format: val => `₹ ${val}`,}],
    }
  },
  methods:{
    calculateInterest(){
      const principalPerMonth = Math.ceil(this.loanItem.amount / this.loanItem.repaymentMonths);
      console.log('principal: ', principalPerMonth)
      const interestPerMonth= Math.ceil(principalPerMonth * this.loanItem.interestPerMonth * 0.01)
      const repayableAmount = principalPerMonth + interestPerMonth;
      for(let i = 1; i<= this.loanItem.repaymentMonths;i++){
        let date = new Date()
        this.rows.push(
          {...this.loanItem,
            repayableAmount : repayableAmount,
            amount:principalPerMonth,
            dueDate: new Date(date.setMonth(date.getMonth() + i)).toLocaleDateString(),
            number:i,
            interestPerMonth: interestPerMonth
          })
      }

    }
  }
}
