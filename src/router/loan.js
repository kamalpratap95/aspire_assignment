const loan = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path :'', name: 'AppDashboard', component:()=> import('pages/dashboard/AppDashboard')},
      {
        path: 'loans',
        component: () => import('pages/loan/index'),
        children:[
          {
            path:'',
            name:'LoanList',
            component: () => import('pages/loan/LoanList')
          },
          {
            path:':id/apply',
            name:'ApplyLoan',
            component: () => import('pages/loan/ApplyLoan')
          },
        ]
      },
      {
        path: 'payments',
        name:'RepayLoan',
        component: () => import('pages/payments/index'),
        children: [
          {
            path:'',
            name:'PurchasedLoanList',
            component : () => import('pages/payments/PurchasedLoansList')
          },
          {
            path:'my-loan/:id',
            name:'PaySchedulePayments',
            component : () => import('pages/payments/PaySchedulePayments')
          },
        ]
      },
      {
        path: 'credits',
        name:'AppCredits',
        component: () => import('pages/credits/AppCredits')
      },
      {
        path: 'cards',
        name:'AppCards',
        component: () => import('pages/cards/AppCards')
      },
      {
        path: 'settings',
        name:'AppSettings',
        component: () => import('pages/settings/AppSettings')
      },

    ]
  },

]

export default loan
