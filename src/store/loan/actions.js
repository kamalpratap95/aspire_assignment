import {api} from 'boot/axios'

export async function fetchLoanList (context ) {
  try{
    const response = await api.get('/loans')
    console.log('response: ', response.data)
    context.commit('loan/SAVE_LOAN_LIST', response.data, {root:true});
    return response.data;
  }
  catch (e){
    console.log("error: ", e)
  }
}

export async function fetchLoanById(context, payload){
  try{
    const response = await api.get(`/loans/${payload}`)
    console.log('actions: ', response.data)
    return response.data;
  }
  catch (e){
    console.log("error: ", e)
  }
}

export async function fetchPurchasedLoanList(context){
  // assuming userId : 1,
  try {
    const response = await api.get(`/purchased-loan-list/1`)
    console.log('purchased loan: ', response.data)
    return response.data;
  }
  catch (e){
    console.log("error: ", e)
  }
}
