import {api} from 'boot/axios'
export async function registerUser (context, payload ) {
  try{
      const response = await api.post(`/users`, payload)
      context.commit('auth/SAVE_USER_INFO', response.data, {root:true});
      return response.data;
  }
  catch (e){
   console.log("error: ", e)
  }
}

export async function loginUser (context ) {
  try{
    // assuming id = 1 and fetching user 1 from db.json
    const response = await api.get('/users/1')
    console.log('response: ', response.data)
    context.commit('auth/SAVE_USER_INFO', response.data, {root:true});
    return response.data;
  }
  catch (e){
    console.log("error: ", e)
  }
}
