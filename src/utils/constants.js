export default Object.freeze({
  REGEX:{
    email :/^(?=[a-zA-Z0-9@._%+-]{6,254}$)[a-zA-Z0-9._%+-]{1,64}@(?:[a-zA-Z0-9-]{1,63}\.){1,8}[a-zA-Z]{2,63}$/,
    mobile:'^((\\\\+91)?|91)?[789][0-9]{9}'
  }
});
